/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;

import pk.labs.Lab9.beans.*;

/**
 *
 * @author MadMike
 */
public class ConsultationListFactoryImpl implements ConsultationListFactory {

    @Override
    public ConsultationList create() {
        ConsultationList consultationList = new ConsultationListImpl();
        return consultationList;
        
    }

    @Override
    public ConsultationList create(boolean deserialize) {
        return create();
    }

    @Override
    public void save(ConsultationList consultationList) {
        
    }
    
}
