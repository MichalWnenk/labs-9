/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeSupport;
import java.util.ArrayList;
import pk.labs.Lab9.beans.*;

/**
 *
 * @author MadMike
 */
public class ConsultationListImpl implements ConsultationList {
    
    private Consultation[] consultations;
    
    private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    private final VetoableChangeSupport vcs = new VetoableChangeSupport(this);

    public ConsultationListImpl() {
    }
    
    @Override
    public int getSize() {
        return this.consultations.length;
    }

    @Override
    public Consultation[] getConsultation() {
        return this.consultations;
    }

    @Override
    public Consultation getConsultation(int index) {
        return this.consultations[index];
    }

    @Override
    public void addConsultation(Consultation consultation) throws PropertyVetoException {
        
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }
    
}
