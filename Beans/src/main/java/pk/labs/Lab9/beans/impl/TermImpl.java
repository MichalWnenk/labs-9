/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;

import java.util.Date;
import pk.labs.Lab9.beans.*;

/**
 *
 * @author MadMike
 */
public class TermImpl implements Term {
    
    private Date begin;
    private int duration;
    private Date end;
    
    public TermImpl() {
        
    }

    @Override
    public Date getBegin() {
        return this.begin;
    }

    @Override
    public void setBegin(Date begin) {
        this.begin = begin;
    }

    @Override
    public int getDuration() {
        return this.duration;
    }

    @Override
    public void setDuration(int duration) {
        this.duration = duration;
    }

    @Override
    public Date getEnd() {
        return this.end;
    }
    
}
