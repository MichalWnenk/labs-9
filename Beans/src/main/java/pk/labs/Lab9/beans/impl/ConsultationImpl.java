/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;

import java.beans.PropertyVetoException;
import java.util.Date;
import pk.labs.Lab9.beans.*;

/**
 *
 * @author MadMike
 */
public class ConsultationImpl implements Consultation {

    private String student;
    private Date beginDate;
    private Date endDate;
    private Term term;
    
    public ConsultationImpl() {
        
    }
    
    @Override
    public String getStudent() {
        return this.student;
    }

    @Override
    public void setStudent(String student) {
        this.student = student;
    }

    @Override
    public Date getBeginDate() {
        return this.beginDate;
    }

    @Override
    public Date getEndDate() {
        return this.endDate;
    }

    @Override
    public void setTerm(Term term) throws PropertyVetoException {
        if (term == null) {
            throw new PropertyVetoException("", null);
        }
        this.term = term;
    }

    @Override
    public void prolong(int minutes) throws PropertyVetoException {
        if (minutes <= 0) {
           return;
        }
        int duration = term.getDuration();
        duration += minutes; // prolong
        this.term.setDuration(duration);
    }
    
}
